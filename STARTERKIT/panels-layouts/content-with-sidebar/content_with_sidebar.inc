<?php
$plugin = array(
  'title' => t('Content with Sidebar'),
  'category' => t('Zenjy'),
  'icon' => 'icon.png',
  'theme' => 'content_with_sidebar',
  'admin css' => 'content_with_sidebar.css',
  'regions' => array(
    'content' => t('Content'),
    'sidebar' => t('Sidebar'),
    'under_content' => t('Under Content'),
  ),
);
