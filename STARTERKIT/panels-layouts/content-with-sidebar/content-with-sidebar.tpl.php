<div class="content-with-sidebar">
  <div class="content-with-sidebar__content">
    <?php print $content['content']; ?>
  </div>
  <div class="content-with-sidebar__sidebar">
    <?php print $content['sidebar']; ?>
  </div>
  <div class="content-with-sidebar__under-content">
    <?php print $content['under_content']; ?>
  </div>
</div>
