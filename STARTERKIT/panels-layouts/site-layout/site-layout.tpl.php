<div class="site-layout">
  <div class="site-layout__header header">
    <div class="page-width">
      <?php print $content['header']; ?>
    </div>
  </div>

  <div class="site-layout__navigation">
    <div class="page-width primary-navigation">
      <?php print $content['navigation']; ?>
    </div>
  </div>

  <div class="site-layout__highlighted">
    <div class="page-width">
      <?php print $content['highlighted']; ?>
    </div>
  </div>

  <div class="site-layout__content">
    <div class="page-width">
      <?php print $content['content']; ?>
    </div>
  </div>

  <div class="site-layout__under-content">
    <div class="page-width">
      <?php print $content['under_content']; ?>
    </div>
  </div>

  <div class="site-layout__footer">
    <div class="page-width">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
