<?php
$plugin = array(
  'title' => t('Site Layout'),
  'category' => t('Zenjy'),
  'icon' => 'icon.png',
  'theme' => 'site_layout',
  'admin css' => 'site_layout.css',
  'regions' => array(
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'highlighted' => t('Highlighted'),
    'content' => t('Content'),
    'under_content' => t('Under Content'),
    'footer' => t('Footer'),
  ),
);
